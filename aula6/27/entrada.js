/*
Implemente um pacote capaz de solicitar ao usuário uma sequência de
palavras. Ao final, o programa deve salvar todas as palavras em um
arquivo de texto (de preferência no formato JSON ). No entanto, o
arquivo não poderá conter palavras repetidas ou nulas.
*/

var lst = new Array();

var rl = require('readline-sync');
var ans = null;

do{
    ans = rl.question("Informe uma palavra: ");
    addList(ans);
} while(ans != 0);

function addList(ans) {
    if (ans != '' || ans != null) {
        lst.push(ans);
    }

    lst = lst.filter(function(a, b) {
        return lst.indexOf(a) === b;
    });
};

console.log(lst);

var fs = require('fs');

fs.writeFile("C:\\temp\\file.json", JSON.stringify(lst), function(erro) {

    if(erro) {
        throw erro;
    }

    console.log("Arquivo salvo");
}); 
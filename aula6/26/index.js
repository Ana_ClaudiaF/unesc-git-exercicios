'use strict';
const args = process.argv;
var total = 0;
var numero = 0;

args.forEach(function(val, index, array){
    if (!isNaN(val)){
        numero = Number(val);
        total += (numero % 2 === 0) ? numero : 0;
    }
});

console.log(total);